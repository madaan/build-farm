//sg
#include<QTcpServer>
#include<QTcpSocket>
#include<qdebug.h>
#include<QObject>
#include<fstream>
#include <qfile.h>
#include<QTimer>
#define SERVER_PORT 5900
#define SEND_INTERVAL 1500
/// The server that serves the CPU usage data
class CPUUsageServer :QObject{
    Q_OBJECT
    public :
    CPUUsageServer();
    virtual ~CPUUsageServer();
    public slots :
        void sendUsage();
        void handleConnection();
       private : 
    QTcpServer * server;
    QTcpSocket * client;
    void  getUsage();
    QFile * statsFile;
    QTextStream * stats;
    QTimer * sendTimer;
    float * usageData;
    int numCores;
    void getNumCores();
    /*Data structures to save the usage of each of the cores*/
    long long * userT1, *niceT1, *systemT1, *idleT1;
    long long * userT2, *niceT2, *systemT2, *idleT2;
    void setUpArrays();
    /*_____________________________________________________*/
    
};


