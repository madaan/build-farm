//sg
#include "cpuusageserver.h"
#include<QApplication>
#include<QDebug>
#include <qvarlengtharray.h>
#include<QStringList>


CPUUsageServer::CPUUsageServer()
{
    client = NULL;
    numCores = 0;
    server = new QTcpServer();
    statsFile = new QFile("/proc/stat");
    stats = new QTextStream(statsFile);
    getNumCores();
    setUpArrays();
    sendTimer = new QTimer(this);

    if (!statsFile->open(QIODevice::ReadOnly | QIODevice::Text))
         qDebug() << "Error opening file";
    if(!server->listen(QHostAddress::Any, SERVER_PORT)) {
        qDebug()<<"Unable to listen";
    }
    connect(server, SIGNAL(newConnection()), this, SLOT(handleConnection()));
    sendTimer->start(SEND_INTERVAL);
    qDebug()<<"The server";

}


void CPUUsageServer::handleConnection()
{
        connect(sendTimer, SIGNAL(timeout()), this, SLOT(sendUsage())); 
        client = server->nextPendingConnection();
}

void CPUUsageServer::sendUsage()
{
 /*  USED FOR SENDING SUMMARY DATA. DO NOT UNCOMMENT
 QString usage = QString::number(getUsage());
 qDebug() << "Sending " << (usage.toLatin1());
 QByteArray usagedump = usage.toLatin1();
 if(client) {
 client->write(usagedump);
 client->waitForBytesWritten();
 }
 */

    getUsage();
    QString usage = "";

    /// the usage is sent as a string in the format : 
    /// core1usage; core2usage; ... ;corNusage. This simplifies parsing

    for(int i = 0; i < numCores; i++) {
        usage += QString::number(usageData[i]);
        usage += ';';
        qDebug()<<QString::number(usageData[i]);
    }
    QByteArray usagedump = usage.toLatin1();
    if(client) {
        qDebug()<<"Sending : "<<usage;
        client->write(usagedump);
        client->waitForBytesWritten();
    }

}


///this function calculates the usage of each of the cores
/// and populates the array with the values
void CPUUsageServer::getUsage()
{
    QString usageLine;
    QStringList usageList; 
    QString name;
    float num, den;
    int i = 0;
    stats->readLine(); //skip the first line
    while( i < numCores ) {
    *stats>>name;
    *stats>>userT2[i];
    *stats>>niceT2[i];
    *stats>>systemT2[i];
    *stats>>idleT2[i];
    stats->readLine();
    num = (userT2[i] - userT1[i]) + (niceT2[i] - niceT1[i]) + (systemT2[i] - systemT1[i]);
    den = num + (idleT2[i] - idleT1[i]);
    qDebug() << "num : " << num <<"den : "<<den;
    userT1[i] = userT2[i];
    niceT1[i] = niceT2[i];
    systemT1[i] = systemT2[i];
    idleT1[i] = idleT2[i];
    if(den == 0) {
        usageData[i] = 0.0f;
    }
    usageData[i] = (num / den) * 100;
    i++;
  } //end while

    stats->seek(0); //for next time
  //  return (static_cast<float>(num) / den) * 100;
}

CPUUsageServer::~CPUUsageServer()
{
 statsFile->close();
}


///this function sets up the arrays that are supposed to store the cpu usage
///assumes that numCores has been calculated
void CPUUsageServer::setUpArrays()
{
    userT1 = new long long[numCores];
    systemT1 = new long long[numCores];
    idleT1 = new long long[numCores];
    niceT1 = new long long[numCores];
    userT2 = new long long[numCores];
    systemT2 = new long long[numCores];
    idleT2 = new long long[numCores];
    niceT2 = new long long[numCores];
    usageData = new float[numCores];

    for(int i = 0; i < numCores; i++) {
        userT1[i] = 0;
        systemT1[i] = 0;
        idleT1[i] = 0;
        niceT1[i] = 0;
        usageData[i] = 0.0f;
    }
}

void CPUUsageServer::getNumCores()
{
    QString temp;
    if (!statsFile->open(QIODevice::ReadOnly | QIODevice::Text))
         qDebug() << "Error opening file ";

while(1) { //forever and ever
    temp = stats->readLine();
    if(temp.at(0) == 'c') {
        numCores++;
    }
    else{
        break;
    }

    }
    statsFile->close();
    numCores--; //discouting for the first summary line
    qDebug()<<"Total number of cores : "<<numCores;
}
int main(int argc, char * argv[])
{
    QApplication app(argc,argv);
    CPUUsageServer cserver;
    return app.exec();

}

