/****************************************************************************
** Meta object code from reading C++ file 'cpuusageserver.h'
**
** Created: Wed May 1 15:46:50 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "cpuusageserver.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cpuusageserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CPUUsageServer[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,
      28,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CPUUsageServer[] = {
    "CPUUsageServer\0\0sendUsage()\0"
    "handleConnection()\0"
};

const QMetaObject CPUUsageServer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CPUUsageServer,
      qt_meta_data_CPUUsageServer, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CPUUsageServer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CPUUsageServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CPUUsageServer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CPUUsageServer))
        return static_cast<void*>(const_cast< CPUUsageServer*>(this));
    return QObject::qt_metacast(_clname);
}

int CPUUsageServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: sendUsage(); break;
        case 1: handleConnection(); break;
        default: ;
        }
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
