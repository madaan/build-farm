//sg
#include <QtNetwork/qtcpsocket.h>
#include<QObject>
#include <qsignalmapper.h>
#include <qtimer.h>
#include <qfile.h>
#define MAX_SERVERS 100
#define MAX_CORES 400 
#define SERVER_PORT 5900
#define XML_GENERATION_INTERVAL 1000
#define ALIVE_LIST_UPDATION_INTERVAL 3000
class UsageCollector :QObject {
 Q_OBJECT
 public:
    UsageCollector();
    virtual ~UsageCollector();
public slots: 
    void collect(int);
    void dumpXMLToFile();
    //void updateAliveServerList();
    void setAllServersDead();
private : 

    void fillSockets();
    QTcpSocket *collectorSockets[MAX_SERVERS];
    QString usageData[MAX_SERVERS];
    QSignalMapper *signalMapper;
    QTimer *generateXMLTimer;
    QTimer *setAllServersDeadTimer;
    int serversPresent; //stores the actual number of servers present
    int dataReceived; //stores the number of servers from which the data has been recieved
    QFile *usageXMLFile;
    QTextStream *xmlStream;
    QString serverIP[MAX_SERVERS];
    bool isAlive[MAX_SERVERS];//this map records whether the server is live or not.

};