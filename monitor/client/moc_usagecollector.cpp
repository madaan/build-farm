/****************************************************************************
** Meta object code from reading C++ file 'usagecollector.h'
**
** Created: Fri Apr 26 15:23:22 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.7.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "usagecollector.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'usagecollector.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.7.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_UsageCollector[] = {

 // content:
       5,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x0a,
      29,   15,   15,   15, 0x0a,
      45,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_UsageCollector[] = {
    "UsageCollector\0\0collect(int)\0"
    "dumpXMLToFile()\0setAllServersDead()\0"
};

const QMetaObject UsageCollector::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_UsageCollector,
      qt_meta_data_UsageCollector, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &UsageCollector::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *UsageCollector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *UsageCollector::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_UsageCollector))
        return static_cast<void*>(const_cast< UsageCollector*>(this));
    return QObject::qt_metacast(_clname);
}

int UsageCollector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: collect((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: dumpXMLToFile(); break;
        case 2: setAllServersDead(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
