//sg
#include "usagecollector.h"
#include <QApplication>
#include <qfile.h>
#include<QStringList>

/* CPU Usage data is collected from various sources.
 * the data is stored in an array called mostRecentUsage.
 * At regular intervals, the contents of mostRecentUsage 
 * are dumped out to an xml file*/

UsageCollector::UsageCollector()
{
    signalMapper = new QSignalMapper(this);
    usageXMLFile = new QFile("usage.xml");
    xmlStream = new QTextStream(usageXMLFile);

    generateXMLTimer = new QTimer(this);
    generateXMLTimer->setSingleShot(true);
    generateXMLTimer->start(XML_GENERATION_INTERVAL);

    //setAllServersDeadTimer = new QTimer(this);
    //setAllServersDeadTimer->start(ALIVE_LIST_UPDATION_INTERVAL);
    connect(generateXMLTimer, SIGNAL(timeout()), this, SLOT(dumpXMLToFile()));
    //connect(setAllServersDeadTimer, SIGNAL(timeout()), this, SLOT(setAllServersDead()));
    fillSockets();
}

///function to collect the data sent by servers
/// the server sends the information in the form of a string

void UsageCollector::collect(int source)
{
    if (this->dataReceived == 0) { //the first one to supply data
        generateXMLTimer->start();
    }
    qDebug() << "Source : " << source;
    usageData[source] = QString(collectorSockets[source]->readAll());
    if (!isAlive[source]) {//avoid duplicates
         isAlive[source] = true;
         this->dataReceived++;
    }
    qDebug() << "Got : " << usageData[source];
    if (this->dataReceived == serversPresent) {
        generateXMLTimer->stop(); //stop the timer
        this->dumpXMLToFile();
        this->dataReceived = 0;

    }
}

///function to read the server list and connect sockets
void UsageCollector::fillSockets()
{
    QFile serverListFile("serverlist.config");
    int i = 0;
    if (!serverListFile.open(QIODevice::ReadOnly|QIODevice::Text)) {
        qDebug() << "ERROR OPENING THE SERVER CONFIG FILE";
        return;
    }

    QTextStream sl(&serverListFile);
    while(!sl.atEnd() && i < MAX_SERVERS) {
        sl >> serverIP[i];
        collectorSockets[i] = new QTcpSocket();
        collectorSockets[i]->connectToHost(serverIP[i], SERVER_PORT);
        connect(collectorSockets[i], SIGNAL(readyRead()), signalMapper, SLOT(map()));
        signalMapper->setMapping(collectorSockets[i], i);
        i++;
    }
    serversPresent = i;
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(collect(int)));
    serverListFile.close();
}


///function to generate XML file at regular intervals
///this function simply dumps the contents of the cpuusagedata array
///in an xml file

void UsageCollector::dumpXMLToFile()
{

    QStringList usageList;
    /*if no one has submitted the data yet, don't dump. This
     * makes sure that there is atleast some information in the usage.xml always*/

    if (this->dataReceived == 0) {
        return;
    }
    if (!usageXMLFile->open(QIODevice::ReadWrite|QIODevice::Text|QIODevice::Truncate)) {
    qDebug() << "Error opening file, program will now exit";
    }
    (*xmlStream) << "<xml version = \'1.0\' >\n"        ;
    *xmlStream << "<usagedata>\n";
    for (int i = 0; i < serversPresent; i++) {
    if (!isAlive[i]) { //if there has been no data from the server
        continue;
    }
    *xmlStream << "<info>\n";
    *xmlStream << "<servernumber>" << serverIP[i] << "</servernumber>\n";
    //split the usagedata from each of the servers to 
    //extract information about individual cores
    usageList = usageData[i].split(";");
    foreach(const QString& usage, usageList) {
        if(!usage.isEmpty()) {
            *xmlStream<<"<core>" << usage << "</core>\n";
            }
    }
     *xmlStream<<"</info>\n";
    }
    *xmlStream<<"</usagedata>\n";
    *xmlStream<<"</xml>\n";
    usageXMLFile->close();
    this->dataReceived = 0;
    setAllServersDead();
}

UsageCollector::~UsageCollector()
{
usageXMLFile->close();
}

int main(int argc, char * argv[] )
{
    QApplication app(argc,argv);
    UsageCollector uc;
    return app.exec();
}

void UsageCollector::setAllServersDead()
{
    for(int i = 0; i < MAX_SERVERS; i++) {
        this->isAlive[i] = false;
    }
}