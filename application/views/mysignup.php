
<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Build-Farm  Compilation Hub.</title>
    
    <!--
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
	!-->

    <link href="../../css/css1.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../../css/css2.css" media="all" rel="stylesheet" type="text/css" />

    <link href="../css/css1.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../css/css2.css" media="all" rel="stylesheet" type="text/css" />

<!--
      <script src="https://a248.e.akamai.net/assets.github.com/assets/frameworks-d76b58e749b52bc47a4c46620bf2c320fabe5248.js" type="text/javascript"></script>
      <script src="https://a248.e.akamai.net/assets.github.com/assets/github-80a3b7f6948ed4122df090b89e975d21a9c88297.js" type="text/javascript"></script>
!-->      
      <meta http-equiv="x-pjax-version" content="581859cfff990c09f7c9d19575e3e6f4">

      <meta name="description" content="Build software better, together." />

	<link href="../../css/login-box.css" rel="stylesheet" type="text/css" />
	<link href="../css/login-box.css" rel="stylesheet" type="text/css" />
</head>

  <body class="logged_out marketing windows  env-production  ">

	<div >
		<br/>
		<br/>
	</div>
<!--
	<ul class="top-nav">
    <li class="explore"><a href="https://buildfarm.com/explore">Explore Build-Farm</a></li>
  <li class="search"><a href="https://buildfarm.com/search">Search</a></li>
  <li class="features"><a href="https://buildfarm.com/features">Features</a></li>
    <li class="blog"><a href="https://buildfarm.com/blog">Blog</a></li>
</ul>

!-->
            <div class="header-actions">
                <a class="button primary" href="https://buildfarm.com/signup">Sign up for free</a>
              <a class="button" href="https://buildfarm.com/login">Sign in</a>
            </div>

          </div>


<?php echo validation_errors(); ?>


        <div class="home site" >

  <div class="jumbotron">
    <div class="container">
<?php 

$config = array(
	'class' => 'home-signup'
);
echo form_open('signup1/index',$config);

?>
<!--        <form accept-charset="UTF-8" action="signup1" autocomplete="off" class="home-signup" method="post">!-->
	          <dl class="form">
		<br/>
		<br/>
		<H2 style="align: right">Sign Up</H2>
            <dd>
<?php
$data = array(
	'type' => "text",
	'name' => "name",
	'class' => "textfield",
	'placeholder' => "Your Name",
	'autofocus' => "&quot;autofocus&quot;"
);
echo form_input($data);
?>

<!--<input type="text" name="user[login]" class="textfield" placeholder="Your Name" autofocus=&quot;autofocus&quot;>!-->

		</dd>
          </dl>

          <dl class="form">
            <dd>
<?php
$data = array(
	'type' => "text",
	'name' => "email",
	'class' => "textfield",
	'placeholder' => "Your email"
);
echo form_input($data);
?>

<!--              <input type="text" name="user[email]" class="textfield" placeholder="Your email" >!-->
            </dd>
          </dl>

          <dl class="form">
            <dd>
<?php
$data = array(
	'type' => "password",
	'name' => "password",
	'class' => "textfield",
	'placeholder' => "Create a password"
);
echo form_input($data);
?>
              <!--<input type="password" name="user[password]" class="textfield" placeholder="Create a password" >!-->
              <p class="note">Tip: use at least one number and at least 7 characters.</p>
            </dd>
          </dl>

          <p class="signup-agreement">
            By clicking on "Sign up for free" below, you agree to the
            <a href="http://help.buildfarm.com/terms" target="_blank">Terms of Service</a> and the
            <a href="http://help.buildfarm.com/privacy" target="_blank">Privacy Policy</a>.
          </p>

<?php
$data = array(
	'type' => "submit",
	'name' => "Sign up for free",
	'value' => "Sign up for free",
	'class' => "button primary button-block",
);
echo form_input($data);
?>

          <!--<button class="button primary button-block" type="submit">Sign up for free</button>!-->


          <p class="plans"><a href="/plans">See plans and pricing</a></p>
</form>

    <div id="ajax-error-message" class="flash flash-error">
      <span class="mini-icon mini-icon-exclamation"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="mini-icon mini-icon-remove-close ajax-error-dismiss"></a>
    </div>

</body>
</html>
