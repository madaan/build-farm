
<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Build-Farm  Compilation Hub.</title>
    
    <!--
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
	!-->

    <link href="../css/css1.css" media="all" rel="stylesheet" type="text/css" />

    <link href="../css/css2.css" media="all" rel="stylesheet" type="text/css" />

<!--
      <script src="https://a248.e.akamai.net/assets.github.com/assets/frameworks-d76b58e749b52bc47a4c46620bf2c320fabe5248.js" type="text/javascript"></script>
      <script src="https://a248.e.akamai.net/assets.github.com/assets/github-80a3b7f6948ed4122df090b89e975d21a9c88297.js" type="text/javascript"></script>
!-->      
      <meta http-equiv="x-pjax-version" content="581859cfff990c09f7c9d19575e3e6f4">

      <meta name="description" content="Build software better, together." />

	<link href="../css/login-box.css" rel="stylesheet" type="text/css" />
</head>

  <body class="logged_out marketing windows  env-production  ">

	<div >
		<br/>
		<br/>
	</div>
<!--
	<ul class="top-nav">
    <li class="explore"><a href="https://buildfarm.com/explore">Explore Build-Farm</a></li>
  <li class="search"><a href="https://buildfarm.com/search">Search</a></li>
  <li class="features"><a href="https://buildfarm.com/features">Features</a></li>
    <li class="blog"><a href="https://buildfarm.com/blog">Blog</a></li>
</ul>

!-->
            <div class = "header-actions">
                <a class = "button primary" href = "./signup1">Sign up for free</a>
              <a class = "button" href = "./validate">Sign in</a>
            </div>

          </div>

<div style = "padding: 50px 10px 10px 400px;">
	<div class = "error" style = "color:red;font-size:20px;">
		<?php echo validation_errors(); ?>
	
		<?php echo $error ?>
	</div>

<div id="login-box">

	<H2>Login</H2>
	<br />
<?php 
echo form_open('validate');
?>
<!--	<form accept-charset="UTF-8" action="validate" autocomplete="off" method="post">!-->
	<div id="login-box-name" style="margin-top:20px;">User ID:</div>
	<div id="login-box-field" style="margin-top:20px;">
		<input name="email" class="form-login" type="text" title="Username" placeholder="User Name" size="30" maxlength="2048" />
	</div>
	<div id="login-box-name">Password:</div>
	<div id="login-box-field">
		<input name="password" type="password" class="form-login" title="Password" placeholder="Password" size="30" maxlength="2048" /></div>
<br />
	<span class="login-box-options"><input type="checkbox" name="1" value="1"> Remember Me </span>
	<br />
	<br />
        <button class="button primary" type="submit" style="margin-left:90px;">Login</button>
<!--	<button type="submit"><img src="../../img/login-btn.png" width="103" height="42" style="margin-left:90px;" /></button>!-->
</form>
</div>

</div>

    <div id="ajax-error-message" class="flash flash-error">
      <span class="mini-icon mini-icon-exclamation"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="mini-icon mini-icon-remove-close ajax-error-dismiss"></a>
    </div>

    
 

</body>
</html>
