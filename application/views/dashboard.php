<!--sg-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Build-Farm</title>
<link rel="stylesheet" type="text/css" href="http://aman/build-farm/css/dashboard.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://aman/build-farm/css/dashboard1.css" media="all" />
<link rel="stylesheet" type="text/css"
href="http://aman/build-farm/css/usagetablestyle.css" />
<script src = "http://aman/build-farm/application/js/jquery-1.9.1.js" type =
"text/javascript"></script>
<script src = "http://aman/build-farm/application/js/xmltonumbers.js" type =
"text/javascript"></script>
<script src="http://aman/build-farm/application/js/jquery.flot.js" type=
"text/javascript" language="javascript"></script>
<script src="http://aman/build-farm/application/js/jquery.flot.resize.js" 
type="text/javascript"language="javascript"></script>
<script src="http://aman/build-farm/application/js/jquery.flot.stack.js" 
type="text/javascript"language="javascript"></script>
<script src="http://aman/build-farm/application/js/dashboardhandler.js"
type="text/javascript"language="javascript"></script>
<!--script
    src="http://rawgithub.com/ajaxorg/ace-builds/master/src-noconflict/ace.js"
    type="text/javascript" charset="utf-8"></script-->
<script src="http://aman/build-farm/application/js/ace.js"
    type="text/javascript" charset="utf-8"></script>
</head>

<body onload = "init()">

<div id="header">
        <div id="logo">
        <a href="http://aman/build-farm/index.php/db"><img src="http://aman/build-farm/img/logo.gif" alt="" /></a>
		</div>
		<ul>
			<li id = "homebutton" class="c4"><a><span style="cursor:pointer">home</span></a></li>
			<li id = "graphbutton" class="c4"><a><span style="cursor:pointer">usage</span></a></li>
			<li id ="generatebutton" class="c4"><a><span style="cursor:pointer">generate script</span></a></li>
			<li class="c4"><a href="http://aman/build-farm/index.php/logout"><span style="cursor:pointer">logout</span></a></li>
		</ul>
	</div>
    
<div id="body">
		<div id = "homecontent" class="about">
			<h1>about</h1>
			<div>
				<h2>what exactly is build-farm</h2>
				<p>Build-Farm is currently a free of cost “Infrastructure As A Service” which provides a grid of heterogeneous systems that may be used to compile or build (as is understood) any software.</p>
			</div>
			<div>
				<h2>How does it work</h2>
				<p>The service is designed in such a way that the user just has to login with a username and password, which once verified redirects to a page where one can build any program residing on user’s system.</p>
			</div>
			<div>
				<h2>Class Apart</h2>
				<p>The feature that makes this project an outstanding and useful one is that it uses Distributed Compilation that divides the work to be run parallely and thus increases the performance by significantly reducing the compilation time.</p>
				<p>For instance, a Linux Kernel that could have taken somewhere around 45 minutes to an hour on an dual-core system actually took hardly 8-10 minutes on two systems that were each loaded with quad-core processors.</p>
				<p>In order to know more about how the project works and what all the user must do to avail the service please go through the interactive video tutorials and sign up to be a member soon.</p>
	   		</div>
		</div>
	</div>

<div id = "generate">
	<div id = "homecontent1" class="desc">
			<h1>steps to start execution</h1>
			<div>
				<h2>number of nodes</h2>
				<p>Select the number of nodes for execution and then click generate to generate and download an object file.</p>
				<p>You have selected: <label id = "number1"><b>0</b></label> <b><label id = "number"></label></b> nodes</p>
			</div>
	</div>
			<div id='cssmenu'>
					<ul>
						<li class='has-sub'><a><span>Select</span></a>
						<ul>
							<li id = 1><a><span>1</span></a></li>
							<li id = 2><a><span>2</span></a></li>
							<li id = 3><a><span>3</span></a></li>
							<li id = 4 class='last'><a><span>4</span></a></li>
						</ul>
						</li>
						<li id = "genscript"><a><span style="cursor:pointer">Generate</span></a></li>
					</ul>
			</div>
		<div id = "homecontent2" class="desc">
			<div>
				<h2>run object file</h2>
				<p>Run the downloaded object file to set up the environment.</p>
				<p>The most common way of then intitiating a distributed build is to execute make with additional options as specified:</p></br>
				<center><p><i><b>make -j[NUMBER_OF_NODES_ALLOCATED + NUMBER_OF_NODES_IN_YOUR_SYSTEM]CC=distcc</b></i></p>
				<div id = "results"></div>
				<center><p><i><b>Build Farm, distributed compilation demystified</b></i></p>
	   		</div>
		</div>
</div>
<!--div id = "editor" style = "height = 100px;width = 100px;float:right;"></div>
<script>
//    var editor = ace.edit("editor");
    //editor.getSession().setMode("ace/mode/javascript");
</script-->

<div id="graph" style="position:relative;top:100px;left:150px;width:70%;height:60%;">graph here</div>
<div id="xmloutput"></div>
</body>

</html>
