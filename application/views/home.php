
<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Build-Farm  Compilation Hub.</title>
    

    <link href="../css/css1.css" media="all" rel="stylesheet" type="text/css" />
    <link href="../css/css2.css" media="all" rel="stylesheet" type="text/css" />

      <meta http-equiv="x-pjax-version" content="581859cfff990c09f7c9d19575e3e6f4">

      <meta name="description" content="Build software better, together." />

<!--	<link href="../../css/login-box.css" rel="stylesheet" type="text/css" />!-->
	<link href="../css/login-box.css" rel="stylesheet" type="text/css" />
</head>

  <body class="logged_out marketing windows  env-production  ">

	<div >
		<br/>
		<br/>
	</div>


          </div>

        <div class="home site" >

  <div class="jumbotron">
    <div class="container">

<?php 

$config = array(
	'class' => 'home-signup'
);
echo form_open('signup1',$config);

?>
<!--        <form accept-charset="UTF-8" action="../signup1/" autocomplete="off" class="home-signup" method="post">!-->
	          <dl class="form">
		<br/>
		<br/>
		<H2 style="align: right">Sign Up</H2>
            <dd><input type="text" name="name" class="textfield" placeholder="Your Name" data-autocheck-url="/signup_check/username" autofocus=&quot;autofocus&quot;></dd>
          </dl>

          <dl class="form">
            <dd>
              <input type="text" name="email" class="textfield" placeholder="Your email" data-autocheck-url="/signup_check/email">
            </dd>
          </dl>

          <dl class="form">
            <dd>
              <input type="password" name="password" class="textfield" placeholder="Create a password" data-autocheck-url="/signup_check/password">
              <p class="note">Tip: use at least one number and at least 7 characters.</p>
            </dd>
          </dl>

          <p class="signup-agreement">
            By clicking on "Sign up for free" below, you agree to the
            <a href="./pages/terms" target="_blank">Terms of Service</a> and the
            <a href="./pages/privacy" target="_blank">Privacy Policy</a>.
          </p>

          <button class="button primary button-block" type="submit">Sign up for free</button>
          <p class="plans"><a href="./pages/plans">See plans and pricing</a></p>


<div class = "error" style = "color:red;font-size:20px;">
	<?php echo validation_errors(); ?>
</div>


</form>



<div style="padding: -10px -10px 10px 10px;">


<div id="login-box">

	<H2>Login</H2>
	<br />

<?php echo form_open('validate'); ?>

<!--	<form accept-charset="UTF-8" action="../validate/" autocomplete="off" method="post">!-->
	<div id="login-box-name" style="margin-top:20px;">User ID:</div>
	<div id="login-box-field" style="margin-top:20px;">
		<input name="email" class="form-login" type="text" title="Username" placeholder="User Name" size="30" maxlength="2048" />
	</div>
	<div id="login-box-name">Password:</div>
	<div id="login-box-field">
		<input name="password" type="password" class="form-login" title="Password" placeholder="Password" size="30" maxlength="2048" /></div>
<br />
	<span class="login-box-options"><input type="checkbox" name="1" value="1"> Remember Me </span>
	<br />
	<br />
        <button class="button primary" type="submit" style="margin-left:90px;">Login</button>
<!--	<button type="submit"><img src="../../img/login-btn.png" width="103" height="42" style="margin-left:90px;" /></button>!-->
</form>
</div>

</div>

</div><!-- /.home -->


    </div>

      <!-- footer -->
      <div id="footer">
  <div class="container clearfix">

      <dl class="footer_nav">
        <dt>buildfarm</dt>
        <dd><a href="./pages/about">About us</a></dd>
        <dd><a href="#">Blog</a></dd>
        <dd><a href="./pages/contact">Contact &amp; support</a></dd>
        <dd><a href="#">buildfarm Enterprise</a></dd>
        <dd><a href="#">Site status</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Documentation</dt>
        <dd><a href="./pages/help">buildfarm Help</a></dd>
        <dd><a href="./pages/tutorial">buildfarm Pages</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>More</dt>
        <dd><a href="#">Training</a></dd>
        <dd><a href="./pages/plans">Plans &amp; pricing</a></dd>
      </dl>

      <hr class="footer-divider">


    <p class="right">&copy; 2013 <span title="0.01065s from fe4.rs.github.com">buildfarm</span>, Inc. All rights reserved.</p>
    <a class="left" href="#">
      <span class="mega-icon mega-icon-invertocat"></span>
    </a>
    <ul id="legal">
        <li><a href="https://buildfarm.com/site/terms">Terms of Service</a></li>
        <li><a href="https://buildfarm.com/site/privacy">Privacy</a></li>
        <li><a href="https://buildfarm.com/security">Security</a></li>
    </ul>

  </div><!-- /.container -->

</div><!-- /.#footer -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-icon mega-icon-normalscreen"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="mini-icon mini-icon-brightness"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="mini-icon mini-icon-exclamation"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="mini-icon mini-icon-remove-close ajax-error-dismiss"></a>
    </div>

    
 

</body>
</html>
