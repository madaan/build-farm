<?php

class profile extends CI_Controller  {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index() { 
		$this->load->helper('form.php');
		$username = $this->input->get_post('username');
		$data = $this->profile_model->get_profile($username);
		$this->load->view('profile.php',$data,$username);
	}

}


?>
