<?php

class editprofile extends CI_Controller  {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model');
	}

	public function index() { 
		$username = $this->input->get_post('username');
		$this->load->helper('form.php');

		$data = $this->profile_model->get_profile($username);

		$this->load->view('updateprofile.php',$data,$username);
	}
	 
	public function update() {
		$username = $this->input->get_post('username');
		//Put the details in Database

		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('mno', 'Mobile Number', 'trim|required|integer|min_length[9]|max_length[11]');
		$this->form_validation->set_rules('enrollno', 'Enrollment Number', 'trim|required|integer|min_length[10]|max_length[12]');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->helper('form');
			$this->load->view('editprofile');			
			//echo "Error";
		}
		
		else
		{			

		$firstname = $this->input->get_post('firstname') or null;
		$lastname = $this->input->get_post('lastname') or null;
		$mno = $this->input->get_post('mno') or null;
		$enrollno = $this->input->get_post('enrollno') or null;


		//$query = "update profile set firstname = '".$firstname."',lastname = '".$lastname."',mno = '".$mno."',enrollno = '".$enrollno."' where username = '".$username."'";

		$data = array(
			'username' => $username,
			'firstname' => $firstname,
			'lastname' => $lastname,
			'mno' => $mno,
			'enrollno' => $enrollno
			);

		$result = $this->profile_model->update_profile($data);
		
		$data = $this->profile_model->get_profile($username);

		//Redirect Back
		$this->load->helper('form.php');
		$data = $this->profile_model->get_profile($username);
		$this->load->view('profile.php',$data,$username);
		}
	}  
}


?>
