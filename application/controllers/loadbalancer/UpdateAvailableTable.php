<?php
/*This script  : 
1. Reads the three usage files. 
2. Calculates the usage information.
3. All those servers where the usage information is > 65% are marked as
   not available.
TODO : 4. All the servers that are present in current usage but not in previous 2
usages are marked as available.
Also is ^ really required?
5. All the servers that are not present in current usage file but were there in
the previous usage files are marked as unavailable.
*/
$uat = new UpdateAvailableTable();
$uat->index();
class UpdateAvailableTable {
    private $current = "http://aman/build-farm/monitor/client/usage.xml";
    private $currentTwoSecondsAgo = "http://aman/build-farm/monitor/client/usage1.xml";
    private $currentFourSecondsAgo = "http://aman/build-farm/monitor/client/usage2.xml";
    private $UPDATE_INTERVAL = 3;
    private $THRESHOLD_USAGE = 65;
    private $SAMPLE_TIME = 2;
    public function index() 
    {
        while (1) { //forever and ever
            //at the end of every iteration, 
            //the database of available servers is updated. 
            $availableServers = Array();
            $availIndex = 0;
            $busyServers = Array();
			echo "\n" . time() ;
            sleep($this->UPDATE_INTERVAL);
            
            
            $usagePerServer = Array();
            $usagePerServer_T = Array();
            $usagePerServer_2T = Array();

            $serverNames = Array();
            $serverNames_T = Array();
            $serverNames_2T = Array();

            echo "\nReading the files";
            $usagearr = $this->readUsageXML($this->current);
            $usagearr_T = $this->readUsageXML($this->currentTwoSecondsAgo);
            $usagearr_2T = $this->readUsageXML($this->currentFourSecondsAgo);

/* The arrays returned have a structure like : 
Array ( [127.127.0.1] => 
    Array ( [0] => 22.8188 [1] => 20.5479 [2] => 11.4865 [3] => 12.1622 ) ) */
            echo "\nGetting the current usage";
            $i = 0;

            foreach($usagearr as $serverIP => $serverinfoarray) {
                $serverNames[$i++] = $serverIP;
                $total = 0;
                foreach($serverinfoarray as $corenumber => $usagevalue) {
                    $total = $total + $usagevalue;
                }


                $usagePerServer[$serverIP] = $total;
            }
            echo "\nGetting usage $this->SAMPLE_TIME ago";
            foreach($usagearr_T as $serverIP => $serverinfoarray) {
                $serverNames_T[$i++] = $serverIP;
                $total = 0;
                foreach($serverinfoarray as $corenumber => $usagevalue) {
                    $total = $total + $usagevalue;
                }
                $usagePerServer_T[$serverIP] = $total;
             }
            echo "\nGetting the usage $this->SAMPLE_TIME * 2 seconds ago";
            foreach($usagearr_2T as $serverIP => $serverinfoarray) {
                $serverNames_2T[$i++] = $serverIP;
                $total = 0;
                foreach($serverinfoarray as $corenumber => $usagevalue) {
                    $total = $total + $usagevalue;
                }
                $usagePerServer_2T[$serverIP] = $total;
             }
             /*till now, we have read the 3 usage files, the latest, usage 2
             seconds earlier, and 4 seconds before current time.
             Now we calculate the average of the load by taking the IP addresses 
             that were present in all the three. 
             */
             $commonIPArray =
             array_intersect_key(array_intersect_key($usagePerServer,
             $usagePerServer_T), $usagePerServer_2T);
             $i = 0;
             foreach($commonIPArray as $IP => $usage) {
                 $usage = ($usagePerServer[$IP] + $usagePerServer_T[$IP]
                 + $usagePerServer_2T[$IP]) / 3;
                 if ($usage < $this->THRESHOLD_USAGE) { 
                     $availableServers[$IP] = $usage;
                 }
             }
             //now delete all the servers that are not in the current but were
             //there in the previous 2 lists 
             $deadServers = array_diff($serverNames_T, $serverNames);
             echo " Available : ";
             print_r($availableServers);
             echo "\n";
			 $this->updateTable($availableServers);
             sleep(.5);
        }

    }
   
   private function updateTable($availableServers)
   {
	   
	   $con = mysqli_connect("localhost", "root", "sg123456aman");
	   $seldb = mysqli_select_db($con, "buildfarm");
       echo "\nUpdating the table\n";
	   // Check connection
	   if (!$con) {
			echo "Failed to connect to MySQL: " . mysql_error();
			return;
	   }

	   $sql = "truncate table available";
	   
	   mysqli_query($con,$sql) or die(mysql_error());
	   
	   foreach($availableServers as $IP => $usage) {
			$sql = "insert into available values ('$IP', '$usage')";
			mysqli_query($con,$sql) or die(mysql_error());
	   }
	   mysqli_close($con);
   }
   
    ///This function reads the usage xml file and returns the result 
    ///in a multidimensional array :
    ///use arr[serverIP][corenumber] to get the usage
    private function readUsageXML($xmlFileName)
    {
        $usage = new SimpleXMLElement($xmlFileName, null, true);
        $arr = array();
        $i = 0;
        foreach($usage->usagedata->info as $infonode) {
            $subarr = array();
            $j = 0;
            foreach($infonode->core as $core) {
                $subarr[$j] =  "$core";
                $j++;
            }
            $arr["$infonode->servernumber"] = $subarr;
        }
    return $arr;
     }
}
	?>
