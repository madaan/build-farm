#!/bin/bash
#this script creates a copy of the usage file, usage.xml at regular intervals.
#3 files exist at all times : 
#usage.xml : current file
#usage_1.xml : usage  $SAMPLETIME seconds earlier
#usage_2.xml : usage 2 X $SAMPLETIME seconds earlier

SAMPLETIME=2;
cd /var/www/myhtml/build-farm/monitor/client

while [ 1 ] 
    do
        echo ""
        date +%s
	if ! [ -f "usage1.xml" ]; then
		echo "Creating Usage1.xml"
    		cp usage.xml usage1.xml
	fi 
	if ! [ -f "usage2.xml" ]; then
		echo "Creating Usage2.xml"
                cp usage.xml usage2.xml
        fi 
	
	cp usage.xml temp1.xml	#create a temporary snapshot
        
	sleep "$SAMPLETIME";
	echo "Updating usage1.xml with temp1.xml"
        echo "Taking snapshot of usage.xml"
	cp temp1.xml usage1.xml	#Update usage1.xml
	cp usage.xml temp2.xml	#Take snapshot of usage.xml

        sleep "$SAMPLETIME";
	echo "Updating usage1.xml with temp2.xml"
        echo "Updating usage2.xml with temp1.xml"
        cp temp2.xml usage1.xml #Update usage1.xml
	cp temp1.xml usage2.xml	#Update usage2.xml
    done
