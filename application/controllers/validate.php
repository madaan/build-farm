<?php

class validate extends CI_Controller {
	
	function index()
	{

		$this->load->library('form_validation');
		$this->load->helper('form');
		
		// field name, error message, validation rules
		//$this->form_validation->set_rules('first_name', 'Name', 'trim|required');
		//$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		//$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
		
		$data = array('error' => null);		

		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('mylogin',$data);			
			//echo "Error";
		}
		else
		{
					
		//$this->load->library('session');
		$this->load->model('login_model');
		$query = $this->login_model->validate();
		
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('email'),
				'is_logged_in' => true
			);
			//$this->session->set_userdata($data);
			
			session_start();
			$_SESSION['username'] = $data['username'];
			$_SESSION['is_logged_in'] = $data['is_logged_in'];

			header('location: ./db');
			//$this->load->view('dashboard');
			//redirect('site/members_area');
		}
		else // incorrect username or password
		{
			$data = array('error' => 'Invalid Email id or password');		
			$this->load->view('mylogin',$data);
		}

		}
	}	
}
