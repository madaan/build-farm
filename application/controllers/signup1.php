<?php

class signup1 extends CI_Controller {
	
	function index()
	{
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('url');

		// field name, error message, validation rules
		//$this->form_validation->set_rules('first_name', 'Name', 'trim|required');
		//$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		//$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->helper('form');
			$this->load->view('home');			
			//echo "Error";
		}
		
		else
		{			
			$this->load->model('login_model');
			//$key = rand(1,100);
			$data = array('name' => $this->input->get_post('name'),
				'username' => $this->input->get_post('email'),
				'password' => md5($this->input->get_post('password'))/*,
				'key' => $key*/
			);
			//$mailid = $this->input->get_post('name');
			//$data1=$this->input->get_post();
			//$this->myMail();
			if($query = $this->login_model->create_member($data))
			{
				$this->load->model('profile_model');
				
				$data1 = array(
					'username' => $data['username'],
					'firstname' => $data['name'],
					);
				$this->profile_model->create_profile($data1);

				$data = array('error' => null);		
				$this->load->view('mylogin',$data);


				//$this->load->view('dashboard');
				//$data['main_content'] = 'signup_successful';
				//$this->load->view('includes/template', $data);
			}
			else
			{
				$this->load->view('mysignup');
				//echo "Unable to Connect";			
			}
		}	
	}

	function myMail()
	{
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.mail.yahoo.com',
			'smtp_port' => 465,
			'smtp_user' => 'buildfarm@yahoo.com',
			'smtp_pass' => 'sg123456aman',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1'
	      );
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->to('ashwani.bvp.gupta@gmail.com');
		//$this->email->to($receiv);

		$this->email->from('buildfarm@yahoo.com', 'Build farm');
		$this->email->subject('Email Verification');
//		$this->email->message('Took me 10 minutes, literally, it was the first google result. Codeigniter makes it so easy.');

//		$str = "Confirm your buildfarm account by clicking on this link.\n http://127.0.0.1/build-farm/index.php/verify/index.php?user=".$receiv."&key=".$key;

		$str = "Hi ";
		$this->email->message($str);

		$status = $this->email->send();

		return $status;

	}
	
}
