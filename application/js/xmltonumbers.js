function getXML() {
$.ajax({
    type: "GET",
    url: "http://aman/build-farm/monitor/client/usage.xml",
    dataType: "xml",
    success: parseXML,
  });
}

function parseXML(xml) {
    var i = 1;
    var j = 1;
    var space = 1;
    var thres = 20;	//Change threshold after which the core usage is critical
    var d1 = [];
    var d2 = [];
    var x;
    $("#xmloutput").text("");
    $(xml).find('info').each(function() {
        $(this).find('core').each(function() {	
            x = $(this).text();	
            if ( x > thres ) {
                d1.push( [ j, thres ] ); 
                d2.push( [ j, x - thres ] );
            } else {
                d1.push( [ j, x ] );
            }
                 j = j + space;
         });

// setup plot
var options = {
        legend: {  
            show: true,
            margin: 10,
            backgroundOpacity: 0.5,
        			},
		series: {
					stack: 1,
					bars: { 
    							show: true, align: "center",
							barWidth: 0.5,
							lineWidth: 0,
    							fill: true, 
    							fillColor: { colors: [ { opacity: 0.8 }, { opacity: 0.2 } ] }
							}
				},
		colors: [ "#00FF00", "#FF0000" ],
        	yaxis: { ticks: 10, min: 0, max: 100, tickFormatter: function (v) { return v + " %"; } },
	        xaxis: { ticks: d1.length, min: 0.5, max: d1.length + 0.5, show: true, 
			 tickFormatter: function (v) { return "Core " + v ; } }
		};

$.plot( $("#graph"), [ d1, d2 ], options );
        i = i + 1;
    });
}

function init() {
    setInterval(getXML,1000);
}
