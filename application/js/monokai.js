  


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ace/lib/ace/theme/monokai.js at master · ajaxorg/ace · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <link rel="logo" type="image/svg" href="http://github-media-downloads.s3.amazonaws.com/github-logo.svg" />
    <link rel="xhr-socket" href="/_sockets">
    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">

    
    
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="pdzbsJPBIjL0mIF9+dQv7erbAbrYhmisaunq4dG6byg=" name="csrf-token" />

    <link href="https://a248.e.akamai.net/assets.github.com/assets/github-3dae77b1344d5de8d1c932d28c3904033713ed8b.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://a248.e.akamai.net/assets.github.com/assets/github2-c50bce4aa3874442adce571e4cbf45b223fb4bdf.css" media="all" rel="stylesheet" type="text/css" />
    


      <script src="https://a248.e.akamai.net/assets.github.com/assets/frameworks-c5555a5a962554bd1e804776ae4fd4f37380681f.js" type="text/javascript"></script>
      <script src="https://a248.e.akamai.net/assets.github.com/assets/github-fa56a7c12e836a8bd6e091b31248011997e91ffe.js" type="text/javascript"></script>
      
      <meta http-equiv="x-pjax-version" content="3c0cb86f68f2a9845756a5617ed83817">

        <link data-pjax-transient rel='permalink' href='/ajaxorg/ace/blob/076eeb8cabc2ffce2159a1f169a1103fd8b7ce58/lib/ace/theme/monokai.js'>
    <meta property="og:title" content="ace"/>
    <meta property="og:type" content="githubog:gitrepository"/>
    <meta property="og:url" content="https://github.com/ajaxorg/ace"/>
    <meta property="og:image" content="https://secure.gravatar.com/avatar/fc7dd0ffdb5290c7e473e08e14a31daa?s=420&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png"/>
    <meta property="og:site_name" content="GitHub"/>
    <meta property="og:description" content="ace - Ajax.org Cloud9 Editor"/>
    <meta property="twitter:card" content="summary"/>
    <meta property="twitter:site" content="@GitHub">
    <meta property="twitter:title" content="ajaxorg/ace"/>

    <meta name="description" content="ace - Ajax.org Cloud9 Editor" />

  <link href="https://github.com/ajaxorg/ace/commits/master.atom" rel="alternate" title="Recent Commits to ace:master" type="application/atom+xml" />

  </head>


  <body class="logged_out page-blob linux vis-public env-production  ">
    <div id="wrapper">

      

      
      
      

      
      <div class="header header-logged-out">
  <div class="container clearfix">

      <a class="header-logo-wordmark" href="https://github.com/">Github</a>

    <div class="header-actions">
        <a class="button primary" href="https://github.com/signup">Sign up for free</a>
      <a class="button" href="https://github.com/login?return_to=%2Fajaxorg%2Face%2Fblob%2Fmaster%2Flib%2Face%2Ftheme%2Fmonokai.js">Sign in</a>
    </div>

      <ul class="top-nav">
          <li class="explore"><a href="https://github.com/explore">Explore GitHub</a></li>
        <li class="search"><a href="https://github.com/search">Search</a></li>
        <li class="features"><a href="https://github.com/features">Features</a></li>
          <li class="blog"><a href="https://github.com/blog">Blog</a></li>
      </ul>

  </div>
</div>


      

      


            <div class="site hfeed" itemscope itemtype="http://schema.org/WebPage">
      <div class="hentry">
        
        <div class="pagehead repohead instapaper_ignore readability-menu ">
          <div class="container">
            <div class="title-actions-bar">
              


<ul class="pagehead-actions">



    <li>
      <a href="/login?return_to=%2Fajaxorg%2Face"
        class="minibutton js-toggler-target star-button entice tooltipped upwards"
        title="You must be signed in to use this feature" rel="nofollow">
        <span class="mini-icon mini-icon-star"></span>Star
      </a>
      <a class="social-count js-social-count" href="/ajaxorg/ace/stargazers">
        5,037
      </a>
    </li>
    <li>
      <a href="/login?return_to=%2Fajaxorg%2Face"
        class="minibutton js-toggler-target fork-button entice tooltipped upwards"
        title="You must be signed in to fork a repository" rel="nofollow">
        <span class="mini-icon mini-icon-fork"></span>Fork
      </a>
      <a href="/ajaxorg/ace/network" class="social-count">
        1,132
      </a>
    </li>
</ul>

              <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
                <span class="repo-label"><span>public</span></span>
                <span class="mega-icon mega-icon-public-repo"></span>
                <span class="author vcard">
                  <a href="/ajaxorg" class="url fn" itemprop="url" rel="author">
                  <span itemprop="title">ajaxorg</span>
                  </a></span> /
                <strong><a href="/ajaxorg/ace" class="js-current-repository">ace</a></strong>
              </h1>
            </div>

            
  <ul class="tabs">
      <li class="pulse-nav"><a href="/ajaxorg/ace/pulse" highlight="pulse" rel="nofollow"><span class="mini-icon mini-icon-pulse"></span></a></li>
    <li><a href="/ajaxorg/ace" class="selected" highlight="repo_source repo_downloads repo_commits repo_tags repo_branches">Code</a></li>
    <li><a href="/ajaxorg/ace/network" highlight="repo_network">Network</a></li>
    <li><a href="/ajaxorg/ace/pulls" highlight="repo_pulls">Pull Requests <span class='counter'>8</span></a></li>

      <li><a href="/ajaxorg/ace/issues" highlight="repo_issues">Issues <span class='counter'>132</span></a></li>

      <li><a href="/ajaxorg/ace/wiki" highlight="repo_wiki">Wiki</a></li>


    <li><a href="/ajaxorg/ace/graphs" highlight="repo_graphs repo_contributors">Graphs</a></li>


  </ul>
  
<div class="tabnav">

  <span class="tabnav-right">
    <ul class="tabnav-tabs">
          <li><a href="/ajaxorg/ace/tags" class="tabnav-tab" highlight="repo_tags">Tags <span class="counter ">8</span></a></li>
    </ul>
    
  </span>

  <div class="tabnav-widget scope">


    <div class="select-menu js-menu-container js-select-menu js-branch-menu">
      <a class="minibutton select-menu-button js-menu-target" data-hotkey="w" data-ref="master">
        <span class="mini-icon mini-icon-branch"></span>
        <i>branch:</i>
        <span class="js-select-button">master</span>
      </a>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container">

        <div class="select-menu-modal">
          <div class="select-menu-header">
            <span class="select-menu-title">Switch branches/tags</span>
            <span class="mini-icon mini-icon-remove-close js-menu-close"></span>
          </div> <!-- /.select-menu-header -->

          <div class="select-menu-filters">
            <div class="select-menu-text-filter">
              <input type="text" id="commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
            </div>
            <div class="select-menu-tabs">
              <ul>
                <li class="select-menu-tab">
                  <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
                </li>
                <li class="select-menu-tab">
                  <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
                </li>
              </ul>
            </div><!-- /.select-menu-tabs -->
          </div><!-- /.select-menu-filters -->

          <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket css-truncate" data-tab-filter="branches">

            <div data-filterable-for="commitish-filter-field" data-filterable-type="substring">

                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/c9_search/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="c9_search" rel="nofollow" title="c9_search">c9_search</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/cleanup/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="cleanup" rel="nofollow" title="cleanup">cleanup</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/clickableannos/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="clickableannos" rel="nofollow" title="clickableannos">clickableannos</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/clipboard_history/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="clipboard_history" rel="nofollow" title="clipboard_history">clipboard_history</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/dart-based-on-c/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="dart-based-on-c" rel="nofollow" title="dart-based-on-c">dart-based-on-c</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/debug-marker-dark/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="debug-marker-dark" rel="nofollow" title="debug-marker-dark">debug-marker-dark</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/devel/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="devel" rel="nofollow" title="devel">devel</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/diet/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="diet" rel="nofollow" title="diet">diet</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/experiment/nativednd/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="experiment/nativednd" rel="nofollow" title="experiment/nativednd">experiment/nativednd</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/feature/codecomplete/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="feature/codecomplete" rel="nofollow" title="feature/codecomplete">feature/codecomplete</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/feature/ioscroll/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="feature/ioscroll" rel="nofollow" title="feature/ioscroll">feature/ioscroll</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/fix-100pct-cpu/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="fix-100pct-cpu" rel="nofollow" title="fix-100pct-cpu">fix-100pct-cpu</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/fix/chrome-osx-crash/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="fix/chrome-osx-crash" rel="nofollow" title="fix/chrome-osx-crash">fix/chrome-osx-crash</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/fix/search/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="fix/search" rel="nofollow" title="fix/search">fix/search</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/fixChromeBoldFonts/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="fixChromeBoldFonts" rel="nofollow" title="fixChromeBoldFonts">fixChromeBoldFonts</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/gh-pages/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="gh-pages" rel="nofollow" title="gh-pages">gh-pages</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/highlighting/tmmodes/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="highlighting/tmmodes" rel="nofollow" title="highlighting/tmmodes">highlighting/tmmodes</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/ipad/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="ipad" rel="nofollow" title="ipad">ipad</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/ipad2/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="ipad2" rel="nofollow" title="ipad2">ipad2</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/issues/327/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="issues/327" rel="nofollow" title="issues/327">issues/327</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/js_highlighting/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="js_highlighting" rel="nofollow" title="js_highlighting">js_highlighting</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/language_tool/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="language_tool" rel="nofollow" title="language_tool">language_tool</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/line_widgets/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="line_widgets" rel="nofollow" title="line_widgets">line_widgets</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target selected">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/master/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="master" rel="nofollow" title="master">master</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/mobile/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="mobile" rel="nofollow" title="mobile">mobile</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/multiselect_paste/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="multiselect_paste" rel="nofollow" title="multiselect_paste">multiselect_paste</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/server-side-rendering/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="server-side-rendering" rel="nofollow" title="server-side-rendering">server-side-rendering</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/static_highlighter/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="static_highlighter" rel="nofollow" title="static_highlighter">static_highlighter</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/statusbar/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="statusbar" rel="nofollow" title="statusbar">statusbar</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/theme/update/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="theme/update" rel="nofollow" title="theme/update">theme/update</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/tmlanguage/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="tmlanguage" rel="nofollow" title="tmlanguage">tmlanguage</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/toggleComment/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="toggleComment" rel="nofollow" title="toggleComment">toggleComment</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/tree/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="tree" rel="nofollow" title="tree">tree</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/varchar/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="varchar" rel="nofollow" title="varchar">varchar</a>
                </div> <!-- /.select-menu-item -->
            </div>

              <div class="select-menu-no-results">Nothing to show</div>
          </div> <!-- /.select-menu-list -->


          <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket css-truncate" data-tab-filter="tags">
            <div data-filterable-for="commitish-filter-field" data-filterable-type="substring">

                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.2.0/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.2.0" rel="nofollow" title="v0.2.0">v0.2.0</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.1.6/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.6" rel="nofollow" title="v0.1.6">v0.1.6</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.1.5/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.5" rel="nofollow" title="v0.1.5">v0.1.5</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.1.4/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.4" rel="nofollow" title="v0.1.4">v0.1.4</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.1.3/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.3" rel="nofollow" title="v0.1.3">v0.1.3</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.1.1/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.1" rel="nofollow" title="v0.1.1">v0.1.1</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/v0.1.0/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="v0.1.0" rel="nofollow" title="v0.1.0">v0.1.0</a>
                </div> <!-- /.select-menu-item -->
                <div class="select-menu-item js-navigation-item js-navigation-target ">
                  <span class="select-menu-item-icon mini-icon mini-icon-confirm"></span>
                  <a href="/ajaxorg/ace/blob/cloud9/lib/ace/theme/monokai.js" class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target" data-name="cloud9" rel="nofollow" title="cloud9">cloud9</a>
                </div> <!-- /.select-menu-item -->
            </div>

            <div class="select-menu-no-results">Nothing to show</div>

          </div> <!-- /.select-menu-list -->

        </div> <!-- /.select-menu-modal -->
      </div> <!-- /.select-menu-modal-holder -->
    </div> <!-- /.select-menu -->

  </div> <!-- /.scope -->

  <ul class="tabnav-tabs">
    <li><a href="/ajaxorg/ace" class="selected tabnav-tab" highlight="repo_source">Files</a></li>
    <li><a href="/ajaxorg/ace/commits/master" class="tabnav-tab" highlight="repo_commits">Commits</a></li>
    <li><a href="/ajaxorg/ace/branches" class="tabnav-tab" highlight="repo_branches" rel="nofollow">Branches <span class="counter ">34</span></a></li>
  </ul>

</div>

  
  
  


            
          </div>
        </div><!-- /.repohead -->

        <div id="js-repo-pjax-container" class="container context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:66f6f66f49edcce4bf6db154cedf1429 -->
<!-- blob contrib frag key: views10/v8/blob_contributors:v21:66f6f66f49edcce4bf6db154cedf1429 -->


<div id="slider">
    <div class="frame-meta">

      <p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

        <div class="breadcrumb">
          <span class='bold'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/ajaxorg/ace" class="js-slide-to" data-branch="master" data-direction="back" itemscope="url"><span itemprop="title">ace</span></a></span></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/ajaxorg/ace/tree/master/lib" class="js-slide-to" data-branch="master" data-direction="back" itemscope="url"><span itemprop="title">lib</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/ajaxorg/ace/tree/master/lib/ace" class="js-slide-to" data-branch="master" data-direction="back" itemscope="url"><span itemprop="title">ace</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/ajaxorg/ace/tree/master/lib/ace/theme" class="js-slide-to" data-branch="master" data-direction="back" itemscope="url"><span itemprop="title">theme</span></a></span><span class="separator"> / </span><strong class="final-path">monokai.js</strong> <span class="js-zeroclipboard zeroclipboard-button" data-clipboard-text="lib/ace/theme/monokai.js" data-copied-hint="copied!" title="copy to clipboard"><span class="mini-icon mini-icon-clipboard"></span></span>
        </div>

      <a href="/ajaxorg/ace/find/master" class="js-slide-to" data-hotkey="t" style="display:none">Show File Finder</a>


        
  <div class="commit file-history-tease">
    <img class="main-avatar" height="24" src="https://secure.gravatar.com/avatar/c2b8bf608d92a99998648b0215e5f260?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
    <span class="author"><a href="/nightwing" rel="author">nightwing</a></span>
    <time class="js-relative-date" datetime="2012-10-13T06:46:44-07:00" title="2012-10-13 06:46:44">October 13, 2012</time>
    <div class="commit-title">
        <a href="/ajaxorg/ace/commit/872125db46c7f1d44989710a2967d1f55109b681" class="message">use relative paths everywhere</a>
    </div>

    <div class="participation">
      <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>11</strong> contributors</a></p>
          <a class="avatar tooltipped downwards" title="fjakobs" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=fjakobs"><img height="20" src="https://secure.gravatar.com/avatar/05d0b094455964dc1d8e6c2ece4c27fe?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="nightwing" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=nightwing"><img height="20" src="https://secure.gravatar.com/avatar/c2b8bf608d92a99998648b0215e5f260?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="joewalker" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=joewalker"><img height="20" src="https://secure.gravatar.com/avatar/a2880244d2f36e856b1776f491ff9194?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="gjtorikian" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=gjtorikian"><img height="20" src="https://secure.gravatar.com/avatar/befd819b3fced8c6bd3dba7e633dd068?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="janjongboom" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=janjongboom"><img height="20" src="https://secure.gravatar.com/avatar/672b033bf66f18d40cff3278f12f8f90?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="Gozala" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=Gozala"><img height="20" src="https://secure.gravatar.com/avatar/9289db62d5c34afb4f277a5215e05c83?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="lennartcl" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=lennartcl"><img height="20" src="https://secure.gravatar.com/avatar/586c5fb4d6fa988552d1b8c594f79f1b?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="mikedeboer" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=mikedeboer"><img height="20" src="https://secure.gravatar.com/avatar/0a996f0fe2727ef1668bdb11897e4459?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="mattpardee" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=mattpardee"><img height="20" src="https://secure.gravatar.com/avatar/c35489faf0667a209d76d72123622e81?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="javruben" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=javruben"><img height="20" src="https://secure.gravatar.com/avatar/1473ab9d32f395252f9b824cbd74a50e?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>
    <a class="avatar tooltipped downwards" title="zefhemel" href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js?author=zefhemel"><img height="20" src="https://secure.gravatar.com/avatar/29df001d75f7035a2dd7904fe8717236?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="20" /></a>


    </div>
    <div id="blob_contributors_box" style="display:none">
      <h2>Users on GitHub who have contributed to this file</h2>
      <ul class="facebox-user-list">
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/05d0b094455964dc1d8e6c2ece4c27fe?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/fjakobs">fjakobs</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/c2b8bf608d92a99998648b0215e5f260?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/nightwing">nightwing</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/a2880244d2f36e856b1776f491ff9194?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/joewalker">joewalker</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/befd819b3fced8c6bd3dba7e633dd068?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/gjtorikian">gjtorikian</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/672b033bf66f18d40cff3278f12f8f90?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/janjongboom">janjongboom</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/9289db62d5c34afb4f277a5215e05c83?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/Gozala">Gozala</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/586c5fb4d6fa988552d1b8c594f79f1b?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/lennartcl">lennartcl</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/0a996f0fe2727ef1668bdb11897e4459?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/mikedeboer">mikedeboer</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/c35489faf0667a209d76d72123622e81?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/mattpardee">mattpardee</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/1473ab9d32f395252f9b824cbd74a50e?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/javruben">javruben</a>
        </li>
        <li>
          <img height="24" src="https://secure.gravatar.com/avatar/29df001d75f7035a2dd7904fe8717236?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-user-420.png" width="24" />
          <a href="/zefhemel">zefhemel</a>
        </li>
      </ul>
    </div>
  </div>


    </div><!-- ./.frame-meta -->

    <div class="frames">
      <div class="frame" data-permalink-url="/ajaxorg/ace/blob/076eeb8cabc2ffce2159a1f169a1103fd8b7ce58/lib/ace/theme/monokai.js" data-title="ace/lib/ace/theme/monokai.js at master · ajaxorg/ace · GitHub" data-type="blob">

        <div id="files" class="bubble">
          <div class="file">
            <div class="meta">
              <div class="info">
                <span class="icon"><b class="mini-icon mini-icon-text-file"></b></span>
                <span class="mode" title="File Mode">file</span>
                  <span>40 lines (36 sloc)</span>
                <span>1.936 kb</span>
              </div>
              <div class="actions">
                <div class="button-group">
                      <a class="minibutton js-entice" href=""
                         data-entice="You must be signed in and on a branch to make or propose changes">Edit</a>
                  <a href="/ajaxorg/ace/raw/master/lib/ace/theme/monokai.js" class="button minibutton " id="raw-url">Raw</a>
                    <a href="/ajaxorg/ace/blame/master/lib/ace/theme/monokai.js" class="button minibutton ">Blame</a>
                  <a href="/ajaxorg/ace/commits/master/lib/ace/theme/monokai.js" class="button minibutton " rel="nofollow">History</a>
                </div><!-- /.button-group -->
              </div><!-- /.actions -->

            </div>
                <div class="blob-wrapper data type-javascript js-blob-data">
      <table class="file-code file-diff">
        <tr class="file-code-line">
          <td class="blob-line-nums">
            <span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>

          </td>
          <td class="blob-line-code">
                  <div class="highlight"><pre><div class='line' id='LC1'><span class="cm">/* ***** BEGIN LICENSE BLOCK *****</span></div><div class='line' id='LC2'><span class="cm"> * Distributed under the BSD license:</span></div><div class='line' id='LC3'><span class="cm"> *</span></div><div class='line' id='LC4'><span class="cm"> * Copyright (c) 2010, Ajax.org B.V.</span></div><div class='line' id='LC5'><span class="cm"> * All rights reserved.</span></div><div class='line' id='LC6'><span class="cm"> * </span></div><div class='line' id='LC7'><span class="cm"> * Redistribution and use in source and binary forms, with or without</span></div><div class='line' id='LC8'><span class="cm"> * modification, are permitted provided that the following conditions are met:</span></div><div class='line' id='LC9'><span class="cm"> *     * Redistributions of source code must retain the above copyright</span></div><div class='line' id='LC10'><span class="cm"> *       notice, this list of conditions and the following disclaimer.</span></div><div class='line' id='LC11'><span class="cm"> *     * Redistributions in binary form must reproduce the above copyright</span></div><div class='line' id='LC12'><span class="cm"> *       notice, this list of conditions and the following disclaimer in the</span></div><div class='line' id='LC13'><span class="cm"> *       documentation and/or other materials provided with the distribution.</span></div><div class='line' id='LC14'><span class="cm"> *     * Neither the name of Ajax.org B.V. nor the</span></div><div class='line' id='LC15'><span class="cm"> *       names of its contributors may be used to endorse or promote products</span></div><div class='line' id='LC16'><span class="cm"> *       derived from this software without specific prior written permission.</span></div><div class='line' id='LC17'><span class="cm"> * </span></div><div class='line' id='LC18'><span class="cm"> * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND</span></div><div class='line' id='LC19'><span class="cm"> * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED</span></div><div class='line' id='LC20'><span class="cm"> * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE</span></div><div class='line' id='LC21'><span class="cm"> * DISCLAIMED. IN NO EVENT SHALL AJAX.ORG B.V. BE LIABLE FOR ANY</span></div><div class='line' id='LC22'><span class="cm"> * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES</span></div><div class='line' id='LC23'><span class="cm"> * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;</span></div><div class='line' id='LC24'><span class="cm"> * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND</span></div><div class='line' id='LC25'><span class="cm"> * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT</span></div><div class='line' id='LC26'><span class="cm"> * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS</span></div><div class='line' id='LC27'><span class="cm"> * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</span></div><div class='line' id='LC28'><span class="cm"> *</span></div><div class='line' id='LC29'><span class="cm"> * ***** END LICENSE BLOCK ***** */</span></div><div class='line' id='LC30'><br/></div><div class='line' id='LC31'><span class="nx">define</span><span class="p">(</span><span class="kd">function</span><span class="p">(</span><span class="nx">require</span><span class="p">,</span> <span class="nx">exports</span><span class="p">,</span> <span class="nx">module</span><span class="p">)</span> <span class="p">{</span></div><div class='line' id='LC32'><br/></div><div class='line' id='LC33'><span class="nx">exports</span><span class="p">.</span><span class="nx">isDark</span> <span class="o">=</span> <span class="kc">true</span><span class="p">;</span></div><div class='line' id='LC34'><span class="nx">exports</span><span class="p">.</span><span class="nx">cssClass</span> <span class="o">=</span> <span class="s2">&quot;ace-monokai&quot;</span><span class="p">;</span></div><div class='line' id='LC35'><span class="nx">exports</span><span class="p">.</span><span class="nx">cssText</span> <span class="o">=</span> <span class="nx">require</span><span class="p">(</span><span class="s2">&quot;../requirejs/text!./monokai.css&quot;</span><span class="p">);</span></div><div class='line' id='LC36'><br/></div><div class='line' id='LC37'><span class="kd">var</span> <span class="nx">dom</span> <span class="o">=</span> <span class="nx">require</span><span class="p">(</span><span class="s2">&quot;../lib/dom&quot;</span><span class="p">);</span></div><div class='line' id='LC38'><span class="nx">dom</span><span class="p">.</span><span class="nx">importCssString</span><span class="p">(</span><span class="nx">exports</span><span class="p">.</span><span class="nx">cssText</span><span class="p">,</span> <span class="nx">exports</span><span class="p">.</span><span class="nx">cssClass</span><span class="p">);</span></div><div class='line' id='LC39'><span class="p">});</span></div></pre></div>
          </td>
        </tr>
      </table>
  </div>

          </div>
        </div>

        <a href="#jump-to-line" rel="facebox" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
        <div id="jump-to-line" style="display:none">
          <h2>Jump to Line</h2>
          <form accept-charset="UTF-8" class="js-jump-to-line-form">
            <input class="textfield js-jump-to-line-field" type="text">
            <div class="full-button">
              <button type="submit" class="button">Go</button>
            </div>
          </form>
        </div>

      </div>
    </div>
</div>

<div id="js-frame-loading-template" class="frame frame-loading large-loading-area" style="display:none;">
  <img class="js-frame-loading-spinner" src="https://a248.e.akamai.net/assets.github.com/images/spinners/octocat-spinner-128.gif?1347543525" height="64" width="64">
</div>


        </div>
      </div>
      <div class="context-overlay"></div>
    </div>

      <div id="footer-push"></div><!-- hack for sticky footer -->
    </div><!-- end of wrapper - hack for sticky footer -->

      <!-- footer -->
      <div id="footer">
  <div class="container clearfix">

      <dl class="footer_nav">
        <dt>GitHub</dt>
        <dd><a href="https://github.com/about">About us</a></dd>
        <dd><a href="https://github.com/blog">Blog</a></dd>
        <dd><a href="https://github.com/contact">Contact &amp; support</a></dd>
        <dd><a href="http://enterprise.github.com/">GitHub Enterprise</a></dd>
        <dd><a href="http://status.github.com/">Site status</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Applications</dt>
        <dd><a href="http://mac.github.com/">GitHub for Mac</a></dd>
        <dd><a href="http://windows.github.com/">GitHub for Windows</a></dd>
        <dd><a href="http://eclipse.github.com/">GitHub for Eclipse</a></dd>
        <dd><a href="http://mobile.github.com/">GitHub mobile apps</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Services</dt>
        <dd><a href="http://get.gaug.es/">Gauges: Web analytics</a></dd>
        <dd><a href="http://speakerdeck.com">Speaker Deck: Presentations</a></dd>
        <dd><a href="https://gist.github.com">Gist: Code snippets</a></dd>
        <dd><a href="http://jobs.github.com/">Job board</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>Documentation</dt>
        <dd><a href="http://help.github.com/">GitHub Help</a></dd>
        <dd><a href="http://developer.github.com/">Developer API</a></dd>
        <dd><a href="http://github.github.com/github-flavored-markdown/">GitHub Flavored Markdown</a></dd>
        <dd><a href="http://pages.github.com/">GitHub Pages</a></dd>
      </dl>

      <dl class="footer_nav">
        <dt>More</dt>
        <dd><a href="http://training.github.com/">Training</a></dd>
        <dd><a href="https://github.com/edu">Students &amp; teachers</a></dd>
        <dd><a href="http://shop.github.com">The Shop</a></dd>
        <dd><a href="/plans">Plans &amp; pricing</a></dd>
        <dd><a href="http://octodex.github.com/">The Octodex</a></dd>
      </dl>

      <hr class="footer-divider">


    <p class="right">&copy; 2013 <span title="0.07460s from fe17.rs.github.com">GitHub</span>, Inc. All rights reserved.</p>
    <a class="left" href="https://github.com/">
      <span class="mega-icon mega-icon-invertocat"></span>
    </a>
    <ul id="legal">
        <li><a href="https://github.com/site/terms">Terms of Service</a></li>
        <li><a href="https://github.com/site/privacy">Privacy</a></li>
        <li><a href="https://github.com/security">Security</a></li>
    </ul>

  </div><!-- /.container -->

</div><!-- /.#footer -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
          <div class="suggester-container">
              <div class="suggester fullscreen-suggester js-navigation-container" id="fullscreen_suggester"
                 data-url="/ajaxorg/ace/suggestions/commit">
              </div>
          </div>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped leftwards" title="Exit Zen Mode">
      <span class="mega-icon mega-icon-normalscreen"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped leftwards"
      title="Switch themes">
      <span class="mini-icon mini-icon-brightness"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="mini-icon mini-icon-exclamation"></span>
      Something went wrong with that request. Please try again.
      <a href="#" class="mini-icon mini-icon-remove-close ajax-error-dismiss"></a>
    </div>

    
    
    <span id='server_response_time' data-time='0.07509' data-host='fe17'></span>
    
  </body>
</html>

